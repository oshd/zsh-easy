# For the benefit of emacs users: -*- shell-script -*-

# 1=zsh default/custom; 2=grml; 3=grml-large; 4=sunaku; 5=geometry;
# to choose set ZE_PROMPT to the desired value in ~/.zshrc.pre
# for loading themes with zgen, set prompt to 1 (custom)
if [[ $EUID -eq 0 ]]; then
# default root prompt
    SELECT_PROMPT=2
    # check for value from ~/.zshrc.pre
    if [[ $ZE_PROMPT -gt 0 ]]; then
	SELECT_PROMPT=$ZE_PROMPT
    fi
else
    # default user prompt
    # 5 (geometry) is minimal and fast
    SELECT_PROMPT=5
    # check for settings from ~/.zshrc.pre
    if [[ $ZE_PROMPT -gt 0 ]]; then
	SELECT_PROMPT=$ZE_PROMPT
    fi
fi

# set the prompt
case $SELECT_PROMPT in
    1) prompt off ;;
    2) ;;
    3) prompt grml-large ;;
    4) prompt off && source $ZHOME/lib/prompts/sunaku/prompt-sunaku.zsh ;;
    5) prompt off && source $ZHOME/lib/prompts/geometry/geometry.zsh ;;
esac
unset SELECT_PROMPT
