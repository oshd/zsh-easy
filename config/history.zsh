# For the benefit of emacs users: -*- shell-script -*-

# history file size
HISTSIZE=10000
# how much to save in a session (cannot be more than HISTSIZE)
SAVEHIST=5000
# history ignore
export HISTIGNORE="&:ls:[bf]g:exit:reset:clear:cd:cd ..:cd.."
# write history as it is executed (recommended for zsh >= 3.1.6)
is4 && setopt INC_APPEND_HISTORY
# clean up blanks which are meangingless to the shell
setopt HIST_REDUCE_BLANKS

# verify csh-style bang expansions
setopt HIST_VERIFY
# or disable them completely
#setopt NO_BANG_HIST
