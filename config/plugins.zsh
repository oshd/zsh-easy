# For the benefit of emacs users: -*- shell-script -*-

# fish-style autosuggestions
# needs zsh >= 4.3
if [[ $ZE_NOAUTO -ne 1 ]]; then
    is43 && source $ZHOME/lib/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
fi

# fish-style syntax highlighting
# (this should be loaded before history substring search)
# zsh >= 4.3.17
is4317(){
    [[ $ZSH_VERSION == 4.3.<17->* || $ZSH_VERSION == 4.<4->* \
                                  || $ZSH_VERSION == <5->* ]] && return 0
    return 1
}
is4317 && source $ZHOME/lib/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# fish-style substring history search
# alt-p and alt-n for normal history search (from start of command)
# up-arrow and down-arrow for substring search (default keybinding)
# zsh >= 4.3
if is43 ; then
    source $ZHOME/lib/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
    
    # keybinding: substring history search

    # if this default doesn't work for you, put the necessary
    # setup commands in ~/custom.zsh
    # see https://github.com/zsh-users/zsh-history-substring-search
    
    # OPTION 1: for most systems
    zmodload zsh/terminfo
    bindkey "$terminfo[kcuu1]" history-substring-search-up
    bindkey "$terminfo[kcud1]" history-substring-search-down

    # # OPTION 2: for iTerm2 running on Apple MacBook laptops
    # 	zmodload zsh/terminfo
    # 	bindkey "$terminfo[cuu1]" history-substring-search-up
    # 	bindkey "$terminfo[cud1]" history-substring-search-down

    # OPTION 3: for Ubuntu 12.04, Fedora 21, and MacOSX 10.9
    #bindkey '^[[A' history-substring-search-up
    #bindkey '^[[B' history-substring-search-down

    ## EMACS mode ###########################################
    if [[ $EDITOR == *"emacs"* ]]; then
	bindkey -M emacs '^P' history-substring-search-up
	bindkey -M emacs '^N' history-substring-search-down
    fi

    ## VI mode ##############################################
    if [[ $EDITOR == *"vi"* ]]; then
	bindkey -M vicmd 'k' history-substring-search-up
	bindkey -M vicmd 'j' history-substring-search-down
    fi
fi
