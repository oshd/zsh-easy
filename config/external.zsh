# For the benefit of emacs users: -*- shell-script -*-

#
# load some useful external programs (if they are installed)
#

# fasd
# https://github.com/clvv/fasd
if check_com fasd; then
    eval "$(fasd --init auto)"
fi

# src-hilight for less
# https://www.gnu.org/software/src-highlite/
if check_com src-hilite-lesspipe.sh; then
    export LESSOPEN="| `which src-hilite-lesspipe.sh` %s"
    export LESS=' -R '
fi

# alternative colour scheme
# if check_com source-highlight-esc.sh; then
#     export LESSOPEN="| `which source-highlight-esc.sh` %s"
#     export LESS=' -R '
# fi
