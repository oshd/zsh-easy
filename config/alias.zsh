# For the benefit of emacs users: -*- shell-script -*-

# use aliases in ~/.alias (if it exists)
test -f ~/.alias && source ~/.alias

# root only aliases
if [[ $EUID -eq 0 ]]; then
    # live dmesg output
    alias dwatch='watch -n 0.1 "dmesg | tail -n $((LINES-6))"'
fi

# typing errors...
alias 'cd..=cd ..'
alias 'sl=ls'
# search zsh history quickly
alias 'zh=grep --text ~/.zsh_history -e'
# display $PATH nicely
alias 'path=echo -e ${PATH//:/\\n}'
# copy working directory to the clipboard (if xclip is available)
if check_com xclip; then
    alias 'cpwd=pwd | xclip -selection clipboard'
fi

# Global aliases (expand whatever their position)
# possibly dangerous, grml provides an expansion mode instead
# with the key sequence "Ctrl-x ."
# e.g. C <Ctrl-x .>

# e.g. find . E L
#alias -g L='| less'
#alias -g H='| head'
#alias -g S='| sort'
#alias -g T='| tail'
#alias -g N='> /dev/null'
#alias -g E='2> /dev/null'
