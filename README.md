zsh-easy
========
This is an easy to install, clean, and fully functional config for zsh. It provides nice features without going overboard, and you can most likely get a ready to use shell with little to no subsequent configuration by cloning this repo and making a couple of symbolic links. It includes;
 - Good compatibility across operating systems and terminal emulators (thanks to [grml-zsh](https://grml.org/zsh/))
 - [Fish-style syntax highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)
 - [Fish-style partial history search](https://github.com/zsh-users/zsh-history-substring-search)
 - [Fish-style autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)
 - [Clean prompt (with async git info)](https://github.com/geometry-zsh/geometry) (or easily switch to grml prompt)
 - Sane settings for history, command completion, etc (mostly from grml with a few tweaks)
 - Useful basic aliases
 - No auto-updating and optional [zgen](https://github.com/tarjoilija/zgen) installer system for themes/plugins (off by default)

Screencast
----------
[![asciicast](https://asciinema.org/a/185733.png)](https://asciinema.org/a/185733)

Why?
----
I wanted a plain zsh config that would work across different machines and terminals so that I could clone it anywhere. [grml-zsh](https://grml.org/zsh) seems to provide that, but I also wanted a few extra features (without needing an [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh/) style framework, although it can be optionally enabled with [zgen](https://github.com/tarjoilija/zgen)).

Install
-------
Clone the repo recursively to `~/.zsh-easy` and create two symbolic links:
```sh
cd
# clone the repo (and submodules)
git clone --recursive https://gitlab.com/oshd/zsh-easy.git .zsh-easy
# backup your old config
mv .zshrc .zshrc.orig
# link to new base config
ln -s .zsh-easy/config/zshrc .zshrc
# link to zsh-easy customisations
ln -s .zsh-easy/config/zshrc.local .zshrc.local
```
That's it. For customisation create and edit `~/.zsh-easy/custom.zsh`.

Options
-------
It's possible to set some [grml options](https://grml.org/zsh/grmlzshrc.html) in `~/.zshrc.pre`, and you can also set a few zsh-easy options there too. The defaults operate like this (e.g. when nothing has been set):
```sh
ZE_USE_ZGEN=0 # 1 loads the zgen plugin system
              # for themes make sure ZE_PROMPT=1
ZE_PROMPT=5   # 1=custom/zgen; 2=grml, 3=grml-large, 4=sunaku, 5=geometry
              # (for root the default is 2)
ZE_NOAUTO=0   # 1 disables the fish-style autocomplete plugin
```

If `ZE_USE_ZGEN=1` then [zgen](https://github.com/tarjoilija/zgen) loads and does its normal stuff (including storing plugins) in `~/.zgen`. If you wanted, you could then load and use plugins, including from [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh/), e.g.;
```sh
# bring in oh-my-zsh repo (inside ~/.zgen)
zgen oh-my-zsh
# load themes/plugins
zgen oh-my-zsh themes/arrow
zgen oh-my-zsh plugins/sudo
# saves (and overwites) zgen config (~/.zgen/init.zsh)
zgen save
# finished
# theme/plugins will load on startup (as long as ZE_USE_ZGEN=1 in ~/.zshrc.pre)
```

Prompt
------
The default prompt is now [geometry](https://github.com/geometry-zsh/geometry). It can be configured in various ways using environment variables, so you can set those in `~/.zshrc.pre`

Uninstall
---------
You can easily remove everything and return to your previous configuration:
```sh
cd
# delete the zsh-easy repo
rm -fr .zsh-easy
# delete the links
rm .zshrc.local
rm .zshrc
# restore your old zsh config
mv .zshrc.orig .zshrc
# if you used zgen
rm -fr .zgen
```
